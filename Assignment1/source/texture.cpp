#include "texture.h"

vector<Texture*> Texture::textures;

Texture::Texture(string a_filename, string a_name) {
	m_imageData = NULL;
	bool a = loadTGA(a_filename);
	m_name = a_name;
	textures.push_back(this);
}

Texture::~Texture() {
	for (vector<Texture*>::iterator it = textures.begin(); it != textures.end(); ++it) {
		if ((*it) == this) {
			textures.erase(it);
			break;
		}
	}

	if (m_imageData) {
		delete m_imageData;
	}
}

bool Texture::loadTGA(string a_filename) {
	TGA_Header tgaHeader;

	ifstream file(a_filename.data(), std::ios_base::binary);
	if (!file.is_open()) {
		//! error?
		return false;
	}

	if (!file.read((char*)&tgaHeader, sizeof(tgaHeader))) {
		//! error?
		return false;
	}

	if (tgaHeader.m_imageType != 2) {
		//! error?
		return false;
	}

	m_width = tgaHeader.m_imageWidth;
	m_height = tgaHeader.m_imageHeight;
	m_bitsPerPixel = tgaHeader.m_pixelDepth;

	if (m_width <= 0 || m_height <= 0 || (m_bitsPerPixel != 24 && m_bitsPerPixel != 32)) {
		//! error?
		return false;
	}

	GLuint type = GL_RGBA;
	if (m_bitsPerPixel == 24) {
		type = GL_RGB;
	}

	GLuint bytesPerPixel = m_bitsPerPixel / 8;
	GLuint imageSize = m_width * m_height * bytesPerPixel;

	m_imageData = new GLubyte[imageSize];
	if (m_imageData == NULL) {
		//! error?
		return false;
	}

	if (!file.read((char*)m_imageData, imageSize)) {
		//! error?
		delete m_imageData;
		return false;
	}

	// Swap red and blue (BGR --> RGB)
	for (GLuint i = 0; i < (int)imageSize; i += bytesPerPixel) {
		GLuint temp = m_imageData[i];
		m_imageData[i] = m_imageData[i + 2];
		m_imageData[i + 2] = temp;
	}

	createTexture(m_imageData, m_width, m_height, type);
	return true;
}

bool Texture::createTexture(unsigned char *a_imageData, unsigned int a_width, unsigned int a_height, int a_type) {
	glGenTextures(1, &m_textureID);
	glBindTexture(GL_TEXTURE_2D, m_textureID);

	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexImage2D(GL_TEXTURE_2D, 0, a_type, a_width, a_height, 0, a_type, GL_UNSIGNED_BYTE, a_imageData);
	return true;
}