#pragma once

class InputControl {
public:
	static bool keys[512];
	static bool getPressed(int a_key);
	static bool getReleased(int a_key);
	static bool getDown(int a_key);
	static bool getUp(int a_key);

	static void update();

private:
	static bool oldKeys[512];
};

extern InputControl &Input;