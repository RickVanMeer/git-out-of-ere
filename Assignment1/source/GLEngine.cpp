#include "GLEngine.h"

const int g_fontSize = 13;
const int g_fontSpace = 7;

GLEngine::GLEngine() {

}

GLEngine::~GLEngine() {

}

GLvoid GLEngine::exit() {
	delete GLEngine::getEngine();
}

GLEngine* GLEngine::getEngine() {
	static GLEngine *engine = new GLEngine();

	return engine;
}

GLvoid GLEngine::initialize(GLint a_width, GLint a_height) {
	Light::Initialize();

	m_fontTexture = new Texture("../assets/fonts/CourierNew.tga");
	buildTextureFont();
}

GLvoid GLEngine::buildTextureFont() {
	m_fontBase = glGenLists(256);
	glBindTexture(GL_TEXTURE_2D, m_fontTexture->m_textureID);

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE);

	for (int i = 0; i < 256; i++) {
		float cx = (float)(i % 16) / 16.0f;
		float cy = (float)(i / 16) / 16.0f;

		glNewList(m_fontBase + i, GL_COMPILE);
		
		glBegin(GL_QUADS);
			glTexCoord2f(cx,			cy + 0.0625f);	glVertex2i(0, g_fontSize);
			glTexCoord2f(cx + 0.0625f,	cy + 0.0625f);	glVertex2i(g_fontSize, g_fontSize);
			glTexCoord2f(cx + 0.0625f,	cy);				glVertex2i(g_fontSize, 0);
			glTexCoord2f(cx,			cy);				glVertex2i(0, 0);
		glEnd();
		glTranslated(g_fontSpace, 0, 0);

		glEndList();
	}
}

GLvoid GLEngine::drawText(GLint a_x, GLint a_y, const char *a_text, ...) {
	char text[256];

	va_list ap;

	va_start(ap, a_text);
		vsprintf_s(text, a_text, ap);
	va_end(ap);

	glEnable(GL_TEXTURE_2D);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE);
	glBindTexture(GL_TEXTURE_2D, m_fontTexture->m_textureID);

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();

	glLoadIdentity();
	glTranslated(a_x, a_y, 0);
	glListBase(m_fontBase - 32);
	glCallLists(strlen(text), GL_BYTE, text);

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();
}

GLuint GLEngine::getTextWidth(const char *a_text) {
	return ((strlen(a_text) + 1) * g_fontSpace);
}

GLuint GLEngine::getTextHeight(const char *a_text) {
	return (g_fontSize);
}