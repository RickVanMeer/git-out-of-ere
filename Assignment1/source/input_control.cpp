#include "input_control.h"

InputControl &Input = InputControl();

bool InputControl::keys[512] = {};
bool InputControl::oldKeys[512] = {};

void InputControl::update() {
	for (int i = 0; i < 512; i++) oldKeys[i] = keys[i];
}

bool InputControl::getPressed(int a_key) {
	return (keys[a_key] && !oldKeys[a_key]);
}

bool InputControl::getReleased(int a_key) {
	return (!keys[a_key] && oldKeys[a_key]);
}

bool InputControl::getDown(int a_key) {
	return (keys[a_key] && oldKeys[a_key]);
}

bool InputControl::getUp(int a_key) {
	return (!keys[a_key] && !oldKeys[a_key]);
}