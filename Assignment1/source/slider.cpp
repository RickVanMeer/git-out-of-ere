#include "slider.h"

const int TICK_SIZE = 5;

Slider::Slider(string a_label, float a_min, float a_max, int a_positionX, int a_positionY, int a_width, int a_height) :
	Control(a_positionX, a_positionY, a_width, a_height) {
	m_defaultValue = 0.0f;
	m_value = NULL;
	m_min = a_min;
	m_max = a_max;
	m_label = a_label;
	m_isDragging = false;
}

bool Slider::updateControl(MouseState &a_state) {
	Control::updateControl(a_state);

	int x = a_state.m_x;
	int y = a_state.m_y;

	if (m_inside) {
		if (a_state.m_leftButtonDown) {
			m_isDragging = true;
		}

		if (a_state.m_rightButtonDown) {
			*m_value = m_defaultValue;
		}
	}

	if (!(a_state.m_leftButtonDown)) {
		m_isDragging = false;
	}

	if (m_isDragging) {
		*m_value = (float)(x - m_positionX) / (float)m_width * (m_max - m_min) + m_min;
	}

	return m_isDragging;
}

void Slider::drawControl() {
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glDisable(GL_TEXTURE_2D);

	GLfloat left, right, up, bottom;
	left = GLfloat(m_positionX);
	right = GLfloat(m_positionX + m_width);
	up = GLfloat(m_positionY);
	bottom = GLfloat(m_positionY + m_height);

	glColor4f(0.7f, 0.7f, 0.7f, 0.8f);
	glBegin(GL_QUADS);
		glVertex2f(left		, up);
		glVertex2f(right	, up);
		glVertex2f(right	, bottom);
		glVertex2f(left		, bottom);
	glEnd();

	if (m_inside) {
		glColor4f(0.2f, 0.2f, 0.7f, 0.8f);
		glLineWidth(2.0f);
	}
	else {
		glColor4f(0.2f, 0.2f, 0.7f, 0.5f);
		glLineWidth(1.0f);
	}
	
	glBegin(GL_LINE_STRIP);
		glVertex2f(left		, up);
		glVertex2f(right	, up);
		glVertex2f(right	, bottom);
		glVertex2f(left		, bottom);
		glVertex2f(left		, up);
	glEnd();

	if (*m_value > m_max) {
		*m_value = m_max;
	}
	else if (*m_value < m_min) {
		*m_value = m_min;
	}
	int currentX = (int)((*m_value - m_min) / (m_max - m_min) * (m_width - TICK_SIZE) + m_positionX);
	
	glColor4f(0.3f, 0.3f, 1.0f, 0.5f);
	left = currentX + TICK_SIZE;
	right = currentX;
	glBegin(GL_QUADS);
		glVertex2f(left		, up);
		glVertex2f(right	, up);
		glVertex2f(right	, bottom);
		glVertex2f(left		, bottom);
	glEnd();

	glColor4f(0.7f, 0.7f, 0.7f, 1.0f);
	iGLEngine->drawText(m_positionX + 2, m_positionY + 2, m_label.data());
}

string Slider::getType() {
	return "slider";
}

bool Slider::getDown() {
	return false;
}

void Slider::setValue(float *a_value) {
	m_value = a_value;
	if (m_value != NULL) {
		m_defaultValue = *m_value;
	}
}