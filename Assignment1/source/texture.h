#pragma once

#ifdef _WIN32
#define WIN32_LEAN_AND_MEAN
	#include <windows.h>
#endif

#if defined (__APPLE__) && defined (__MACH__)
	#include <OpenGL/gl.h>
	#include <OpenGL/glu.h>
#else
	#include <GL/gl.h>
	#include <GL/glu.h>
#endif

#include <string>
#include <vector>
#include <fstream>

using std::string;
using std::vector;
using std::ifstream;

struct TGA_Header {
	GLubyte	m_IDLength;
	GLubyte	m_colorMapType;
	GLubyte	m_imageType;
	GLubyte m_colorMapSpecification[5];
	GLshort	m_xOrigin;
	GLshort	m_yOrigin;
	GLshort m_imageWidth;
	GLshort m_imageHeight;
	GLubyte m_pixelDepth;
};

class Texture {
public:
	// -structors //
	Texture(string a_filename, string a_name = "");
	~Texture();

	// Members //
	unsigned char	*m_imageData;
	unsigned int	m_bitsPerPixel;
	unsigned int	m_width;
	unsigned int	m_height;
	unsigned int	m_textureID;
	string			m_name;

	static vector<Texture*> textures;
private:
	// Methods //
	bool loadTGA(string a_filename);
	bool createTexture(unsigned char *a_imageData, unsigned int a_width, unsigned int a_height, int a_type);
};