#pragma once

#include "glm.hpp"
#include "gtc/quaternion.hpp"
#include "gtx/quaternion.hpp"

using namespace glm;

class Cube {
public:
	// Getters and setters
	vec3 getPosition();
	mat3 getRotation();

	// Methods
	void init(vec3 a_position, quat a_rotation, float a_invMass, mat3 a_invInertia, vec3 a_initVelocity, vec3 a_initAngularVelocity);
	void tick(float a_deltaTime);
	void applyForceLocal(vec3 a_force, vec3 a_position, float a_deltaTime);
	void applyForceWorld(vec3 a_force, vec3 a_position, float a_deltaTime);
	void reset();

private:
	// Methos
	vec3 integratePosition(vec3 a_position, float a_deltaTime);
	quat integrateRotation(quat a_rotation, float a_deltaTime);

	// Data members
	vec3	m_linearMomentum;
	vec3	m_angularMomentum;
	vec3	m_position;
	quat	m_rotation;
	float	m_invMass;
	mat3	m_invInertia;
	vec3	m_startVelocity;
	vec3	m_startAngularVelocity;
	vec3	m_startPosition;
	quat	m_startRotation;
	float	m_startInvMass;
	mat3	m_startInvInertia;

};