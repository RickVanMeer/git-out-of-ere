#include "cube.h"

vec3 Cube::getPosition() {
	return m_position;
}

mat3 Cube::getRotation() {
	return toMat3(m_rotation);
}

void Cube::init(vec3 a_position, quat a_rotation, float a_invMass, mat3 a_invInertia, vec3 a_initVelocity, vec3 a_initAngularVelocity) {
	m_position = a_position;
	m_rotation = a_rotation;
	m_invMass = a_invMass;
	m_invInertia = a_invInertia;
	m_linearMomentum = a_initVelocity * m_invMass;
	m_angularMomentum = a_initAngularVelocity * a_invInertia;

	// Needed to reset the cube
	m_startPosition = m_position;
	m_startRotation = m_rotation;
	m_startInvMass = m_invMass;
	m_startInvInertia = m_invInertia;
	m_startVelocity = a_initVelocity;
	m_startAngularVelocity = a_initAngularVelocity;
}

void Cube::tick(float a_deltaTime) {

	// Update the position
	vec3 k1, k2, k3, k4;
	k1 = integratePosition(m_position, a_deltaTime);
	k2 = integratePosition(m_position + k1 / 2.0f, a_deltaTime);
	k3 = integratePosition(m_position + k2 / 2.0f, a_deltaTime);
	k4 = integratePosition(m_position + k3 / 2.0f, a_deltaTime);

	m_position = m_position + k1 / 6.0f + k2 / 3.0f + k3 / 3.0f + k4 / 6.0f;

	// Update the rotation
	quat qk1, qk2, qk3, qk4;
	qk1 = integrateRotation(m_rotation, a_deltaTime);
	qk2 = integrateRotation(m_rotation + qk1 / 2.0f, a_deltaTime);
	qk3 = integrateRotation(m_rotation + qk2 / 2.0f, a_deltaTime);
	qk4 = integrateRotation(m_rotation + qk3 / 2.0f, a_deltaTime);

	m_rotation = m_rotation + qk1 / 6.0f + qk2 / 3.0f + qk3 / 3.0f + qk4 / 6.0f;
	m_rotation = normalize(m_rotation);

	// Update the rotation
	//mat3 convertedRotation = toMat3(m_rotation);
	//vec3 angularVelocity = convertedRotation * m_invInertia * transpose(convertedRotation) * m_angularMomentum;
	//quat addedRotQuat = 0.5f * quat(0.0f, angularVelocity.x, angularVelocity.y, angularVelocity.z) * m_rotation;
	//m_rotation = addedRotQuat * a_deltaTime + m_rotation;
	//m_rotation = normalize(m_rotation);

	
	// Matrix rotation
	//vec3 angularVelocity = m_rotation * m_invInertia * transpose(m_rotation) * m_angularMomentum;
	//vec3 tangent, normal, binormal;
	//tangent = cross(angularVelocity, m_rotation[0]);
	//normal = cross(angularVelocity, m_rotation[1]);
	//binormal = cross(angularVelocity, m_rotation[2]);
	//mat3 addedRotation(tangent, normal, binormal);
	//m_rotation += addedRotation * a_deltaTime;

	// Orthonormalize the rotation matrix
	//vec3 orthoTangent, orthoNormal, orthoBinormal;
	//orthoTangent = normalize(m_rotation[0]);
	//orthoNormal = m_rotation[1];
	//if (dot(orthoNormal, orthoTangent) != 0) orthoNormal = orthoNormal * dot(orthoNormal, orthoTangent);
	//orthoNormal = normalize(orthoNormal);
	//orthoBinormal = cross(orthoTangent, orthoNormal);
	//m_rotation = mat3(orthoTangent, orthoNormal, orthoBinormal);
}
vec3 Cube::integratePosition(vec3 a_position, float a_deltaTime) {
	return (a_position - m_position + m_linearMomentum * m_invMass * a_deltaTime);
}

quat Cube::integrateRotation(quat a_rotation, float a_deltaTime) {
	mat3 convertedRotation = toMat3(a_rotation);
	vec3 angularVelocity = convertedRotation * m_invInertia * transpose(convertedRotation) * m_angularMomentum;
	quat addedRotQuat = 0.5f * quat(0.0f, angularVelocity.x, angularVelocity.y, angularVelocity.z) * a_rotation * a_deltaTime;
	return addedRotQuat;
}

void Cube::applyForceLocal(vec3 a_force, vec3 a_position, float a_deltaTime) {
	vec3 collisionPoint = m_rotation * a_position;

	// Apply force via linear momentum
	vec3 force = a_force * abs(dot(a_force, normalize(collisionPoint)));
	m_linearMomentum += force * a_deltaTime;

	// Apply torque via rotational momentum
	vec3 torque = cross(normalize(collisionPoint), a_force);
	m_angularMomentum += torque * a_deltaTime;
}

void Cube::applyForceWorld(vec3 a_force, vec3 a_position, float a_deltaTime) {
	applyForceLocal(a_force, a_position - m_position, a_deltaTime);
}

void Cube::reset() {
	init(m_startPosition, m_startRotation, m_startInvMass, m_startInvInertia, m_startVelocity, m_startAngularVelocity);
}