#pragma once

#include "interface_control.h"

class Button : public Control {
public:
	Button(string a_label, int a_positionX, int a_positionY, int a_width, int a_height);

	// Methods
	virtual bool updateControl(MouseState &a_state);
	virtual void drawControl();
	virtual string getType();
	virtual bool getDown();

protected:
	bool m_down;

	string m_label;
};