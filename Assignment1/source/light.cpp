#include "light.h"

int Light::numLights = 0;
vector<int> Light::availableLights;
vector<Light*> Light::lights;

void Light::Initialize() {
	glGetIntegerv(GL_MAX_LIGHTS, &numLights);
	for (int i = 0; i < numLights; ++i) {
		availableLights.push_back(GL_LIGHT0 + i);
	}
}

Light::Light(LIGHT_TYPE a_lightType) {
	lights.push_back(this);

	if ((int)availableLights.size() > 0) {
		m_lightNum = availableLights[0];
		availableLights.erase(availableLights.begin());

		setVisible(true);
		setLightType(a_lightType);
		setPosition(0, 0, 0);
		setSpotDirection(0, -1, 0);
		setCutoff(45.0f);
		setExponent(12);
		setAmbient(0, 0, 0, 1);
		setDiffuse(1, 1, 1, 1);
		setSpecular(1, 1, 1, 1);
		updateLight();
	}
	else {
		m_lightNum = 0;
		setVisible(false);
	}
}

Light::~Light() {
	if (m_lightNum != 0) {
		availableLights.push_back(m_lightNum);
	}

	for (vector<Light*>::iterator it = lights.begin(); it != lights.end(); ++it) {
		if ((*it) == this) {
			lights.erase(it);
			break;
		}
	}
}

void Light::setVisible(bool a_isVisible) {
	m_visible = a_isVisible;

	if (m_visible) {
		glEnable(m_lightNum);
	}
	else {
		glDisable(m_lightNum);
	}
}

void Light::setDiffuse(float a_red, float a_green, float a_blue, float a_alpha) {
	m_diffuse[0] = a_red;
	m_diffuse[1] = a_green;
	m_diffuse[2] = a_blue;
	m_diffuse[3] = a_alpha;

	glLightfv(m_lightNum, GL_DIFFUSE, m_diffuse);
}

void Light::setAmbient(float a_red, float a_green, float a_blue, float a_alpha) {
	m_ambient[0] = a_red;
	m_ambient[1] = a_green;
	m_ambient[2] = a_blue;
	m_ambient[3] = a_alpha;

	glLightfv(m_lightNum, GL_AMBIENT, m_ambient);
}
void Light::setSpecular(float a_red, float a_green, float a_blue, float a_alpha) {
	m_specularity[0] = a_red;
	m_specularity[1] = a_green;
	m_specularity[2] = a_blue;
	m_specularity[3] = a_alpha;

	glLightfv(m_lightNum, GL_SPECULAR, m_specularity);
}
void Light::setLightType(LIGHT_TYPE a_lightType) {
	m_lightType = a_lightType;

	if (m_lightType == LIGHT_SPOT) {
		m_position[3] = 1.0f;
	}
	else if (m_lightType == LIGHT_POINT) {
		m_position[3] = 1.0f;
		setCutoff(180.0f);
	}
	else if (m_lightType == LIGHT_DIRECTIONAL) {
		m_position[3] = 0.0f;
	}

	updateLight();
}

void Light::setPosition(float a_x, float a_y, float a_z) {
	m_position[0] = a_x;
	m_position[1] = a_y;
	m_position[2] = a_z;

	glLightfv(m_lightNum, GL_POSITION, m_position);
}

void Light::setSpotDirection(float a_x, float a_y, float a_z) {
	m_spotDirection[0] = a_x;
	m_spotDirection[1] = a_y;
	m_spotDirection[2] = a_z;

	glLightfv(m_lightNum, GL_SPOT_DIRECTION, m_spotDirection);
}

void Light::setCutoff(float a_cutOff) {
	m_cutOff = a_cutOff;

	glLightf(m_lightNum, GL_SPOT_CUTOFF, m_cutOff);
}

void Light::setExponent(float a_exponent) {
	m_exponent = a_exponent;

	glLightf(m_lightNum, GL_SPOT_EXPONENT, m_exponent);
}

void Light::setAttenuation(float a_constant, float a_linear, float a_quadretic) {
	glLightf(m_lightNum, GL_CONSTANT_ATTENUATION, a_constant);
	glLightf(m_lightNum, GL_LINEAR_ATTENUATION, a_linear);
	glLightf(m_lightNum, GL_QUADRATIC_ATTENUATION, a_quadretic);
}

int Light::getLightNum() {
	return m_lightNum;
}

void Light::updateLight() {
	glLightfv(m_lightNum, GL_POSITION, m_position);
	glLightfv(m_lightNum, GL_SPOT_DIRECTION, m_spotDirection);
}