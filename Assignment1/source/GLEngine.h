#pragma once

#ifdef _WIN32
#define WIN32_LEAN_AND_MEAN
	#include <windows.h>
#endif

#if defined (__APPLE__) && defined (__MACH__)
	#include <OpenGL/gl.h>
	#include <OpenGL/glu.h>
#else
	#include <GL/gl.h>
	#include <GL/glu.h>
#endif

#include "light.h"
#include "texture.h"

#define iGLEngine GLEngine::getEngine()

class GLEngine {
public:
	// -structors //
	GLEngine();
	~GLEngine();

	// Methods //
	static GLvoid exit();
	static GLEngine* getEngine();
	GLvoid initialize(GLint a_width, GLint a_height);
	GLvoid buildTextureFont();
	GLvoid drawText(GLint a_x, GLint a_y, const char *a_text, ...);

	// Getters and setters //
	GLuint getTextWidth(const char *a_text);
	GLuint getTextHeight(const char *a_text);

private:
	GLuint m_fontBase;
	Texture *m_fontTexture;
};