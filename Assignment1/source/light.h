#pragma once

#ifdef _WIN32
#define WIN32_LEAN_AND_MEAN
	#include <windows.h>
#endif

#if defined (__APPLE__) && defined (__MACH__)
	#include <OpenGL/gl.h>
	#include <OpenGL/glu.h>
#else
	#include <GL/gl.h>
	#include <GL/glu.h>
#endif

#include <vector>
using std::vector;

enum LIGHT_TYPE {
	LIGHT_SPOT,
	LIGHT_POINT,
	LIGHT_DIRECTIONAL
};

class Light {
public:
	// -structors //
	Light(LIGHT_TYPE a_lightType);
	~Light();

	// Getters and setters //
	void setVisible(bool a_isVisible = true);
	void setDiffuse(float a_red, float a_green, float a_blue, float a_alpha);
	void setAmbient(float a_red, float a_green, float a_blue, float a_alpha);
	void setSpecular(float a_red, float a_green, float a_blue, float a_alpha);
	void setLightType(LIGHT_TYPE a_lightType);
	void setPosition(float a_x, float a_y, float a_z);
	void setSpotDirection(float a_x, float a_y, float a_z);
	void setCutoff(float a_cutOff);
	void setExponent(float a_exponent);
	void setAttenuation(float a_constant, float a_linear, float a_quadretic);
	int getLightNum();

	// Methods //
	static void Initialize();
	void updateLight();

	// Statics
	static int numLights;
	static vector<int> availableLights;
	static vector<Light*> lights;

private:
	// Members //
	GLfloat m_position[4];
	GLfloat m_diffuse[4];
	GLfloat m_ambient[4];
	GLfloat m_specularity[4];
	GLfloat m_spotDirection[4];

	float	m_cutOff;
	float	m_exponent;
	bool	m_visible;
	int		m_lightType;
	int		m_lightNum;
};