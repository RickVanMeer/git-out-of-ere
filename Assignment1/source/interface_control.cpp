#include "interface_control.h"

list<Control*> Control::controls;

Control::Control(int a_positionX, int a_positionY, int a_width, int a_height) :
	m_positionX(a_positionX),
	m_positionY(a_positionY),
	m_width(a_width),
	m_height(a_height) {
	controls.push_back(this);
}

Control::~Control() {
	controls.remove(this);
}

bool Control::updateControl(MouseState &a_state) {
	int x = a_state.m_x;
	int y = a_state.m_y;

	m_inside = false;

	if (x >= m_positionX && x <= m_positionX + m_width &&
		y >= m_positionY && y <= m_positionY + m_height) {
		m_inside = true;
	}

	return false;
}

void Control::setPosition(int a_x, int a_y) {
	m_positionX = a_x;
	m_positionY = a_y;
}

void Control::setSize(int a_width, int a_height) {
	m_width = a_width;
	m_height = a_height;
}

int Control::getWidth() {
	return m_width;
}

int Control::getHeight() {
	return m_height;
}


Control *addControl(Control* a_control) {
	static int lastX = 5;
	static int lastY = 5;

	a_control->setPosition(lastX, lastY);
	lastY += a_control->getHeight() + 5;

	return a_control;
}