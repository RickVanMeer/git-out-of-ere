#pragma once

#include "interface_control.h"

#include <vector>

using std::vector;

class ListBox : public Control {
public:
	// - structors //
	ListBox(int a_positionX, int a_positionY, int a_width, int a_height);

	// Methods //
	virtual bool updateControl(MouseState &a_state);
	virtual void drawControl();
	virtual string getType();
	virtual bool getDown();

	void addItem(string a_item);
	void removeItem(int a_index);
	void setCurrent(int a_index);

	// Getters and setters //
	int getIndex();
	int getCount();
protected:
	int m_index;
	vector<string> m_items;
};