#include "GLEngine.h"
#include "SDL.h"
#include "button.h"
#include "listbox.h"
#include "slider.h"
#include "cube.h"
#include "glm.hpp"
#include <gtc/type_ptr.hpp>

#ifdef _WIN32
#define WIN32_LEAN_AND_MEAN
	#include <windows.h>
#endif

#if defined (__APPLE__) && defined (__MACH__)
	#include <OpenGL/gl.h>
	#include <OpenGL/glu.h>
#else
	#include <GL/gl.h>
	#include <GL/glu.h>
#endif

// Constants
const GLsizei WINDOW_WIDTH	= 500;
const GLsizei WINDOW_HEIGHT = 500;
const GLfloat ROTATE_SPEED	= 1.0f;
const GLfloat ZOOM_SPEED	= 0.01f;
const GLint NUM_BUTTONS		= 2;
const GLfloat PHYSICS_STEP	= 1.0f / 30.0f; // 30 fps

// Controls
Control *g_currentControl	= NULL;
MouseState g_mouseState;
Uint8 *g_keys				= NULL;
Control **g_buttonForce		= new Control*[NUM_BUTTONS];
Control *g_resetButton;

// Navigation
bool g_isNavigating		= false;
GLfloat g_rotateX		= 45.0f;
GLfloat g_rotateY		= 45.0f;
GLfloat g_zoom			= -5.0f;

// Cube
Texture *g_textureBox		= NULL;
GLuint g_gridList			= 0;
Cube *g_cube				= new Cube();
vec3 g_forceToApply			= vec3(0.0f, 0.0f, 0.0f);

float g_timePassed			= 0.0f;

GLvoid		establishProjectionMatrix(GLsizei a_width, GLsizei a_height);
GLvoid		initGL(GLsizei a_width, GLsizei a_height);
GLvoid		displayFPS();
GLboolean	checkInput(GLfloat a_deltaTime);
GLvoid		drawScene(GLfloat a_deltaTime);
GLvoid		drawGrid();
GLvoid		drawCube();
GLvoid		drawControls();
GLvoid		setOrtho(GLsizei a_width, GLsizei a_height);
GLvoid		handleButtons(GLfloat a_deltaTime);
GLvoid		drawFixedCube(GLfloat a_size);

GLvoid establishProjectionMatrix(GLsizei a_width, GLsizei a_height) {
	glViewport(0, 0, a_width, a_height);
	
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(45.0f, (GLfloat)a_width / (GLfloat)a_height, 0.1f, 200.0f);
}

GLvoid setOrtho(GLsizei a_width, GLsizei a_height) {
	glViewport(0, 0, a_width, a_height);
	
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	gluOrtho2D(0, a_width, a_height, 0);
}

GLvoid initGL(GLsizei a_width, GLsizei a_height) {
	iGLEngine->initialize(a_width, a_height);

	establishProjectionMatrix(a_width, a_height);

	glShadeModel(GL_SMOOTH);
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);

	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
	glEnable(GL_PERSPECTIVE_CORRECTION_HINT);

	g_textureBox = new Texture("../assets/images/woodenBox.tga", "Surface Texture");

	for (int i = 0; i < NUM_BUTTONS; ++i) {
		string buttonName;
		switch (i) {
		case 0:
			buttonName = "Front Right Top";
			break;
		case 1:
			buttonName = "Front Right Bottom";
			break;
		}

		g_buttonForce[i] = addControl(new Button(buttonName, 0, 0, 150, 30));
	}
	Slider *sliderX = (Slider*)addControl(new Slider("Force.x", -1.0f, 1.0f, 0, 0, 150, 30));
	sliderX->setValue(&g_forceToApply.x);
	Slider *sliderY = (Slider*)addControl(new Slider("Force.y", -1.0f, 1.0f, 0, 0, 150, 30));
	sliderY->setValue(&g_forceToApply.y);
	Slider *sliderZ = (Slider*)addControl(new Slider("Force.z", -1.0f, 1.0f, 0, 0, 150, 30));
	sliderZ->setValue(&g_forceToApply.z);

	g_resetButton = addControl(new Button("Reset", 0, 0, 150, 30));

	//Initilize the cube
	vec3 cubePosition, tangent, normal, binormal, cubeVelocity, cubeAngularVelocity;
	mat3 cubeInertia;
	quat cubeRotation;
	float cubeMass;
	cubePosition			= vec3(0.0f, 0.0f, -20.0f);
	cubeVelocity			= vec3(0.0f, 0.0f, 0.0f);
	cubeAngularVelocity		= vec3(0.0f, 0.0f,  0.0f);
	cubeRotation			= quat();
	cubeInertia				= mat3(1.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 1.0f);
	cubeMass				= 1.0f;
	g_cube->init(cubePosition, cubeRotation, 1 / cubeMass, cubeInertia._inverse(), cubeVelocity, cubeAngularVelocity);
}

GLvoid displayFPS() {
	static long lastTime = SDL_GetTicks();
	static long frames = 0;
	static GLfloat fps = 0.0f;

	int newTime = SDL_GetTicks();

	// Calculate new FPS
	if (newTime - lastTime > 100) {
		float newFPS = ((float)frames / float(newTime - lastTime)) * 1000.0f;
		fps = (fps + newFPS) / 2.0f;

		lastTime = newTime;
		frames = 0;
	}
	frames++;

	//Draw FPS
	iGLEngine->drawText(350, 5, "Render FPS: %.2f", fps);
	iGLEngine->drawText(350, 20, "Physics FPS: %.2f", ceil(1.0f / PHYSICS_STEP));
}

// Draws a textured cube with normals of 1 m^3
GLvoid drawCube()
{
	glBindTexture(GL_TEXTURE_2D, g_textureBox->m_textureID);

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPushMatrix();

	// Draw the thrusters on the cube
	glPushMatrix();
		if (g_keys[SDLK_RIGHT]) glColor3f( 1.0f, 0.0f, 0.0f );
		vec3 pointOfImpactA = (g_cube->getRotation() * vec3( 1.0f, 1.0f, 1.0f ) ) + g_cube->getPosition();
		glTranslatef( pointOfImpactA[0], pointOfImpactA[1], pointOfImpactA[2] );
		mat4 subCubeRotationA(g_cube->getRotation());
		glMultMatrixf(glm::value_ptr(subCubeRotationA));
		drawFixedCube(0.25f);
		glColor3f(1,1,1);
	glPopMatrix();
		
	glPushMatrix();
		if (g_keys[SDLK_LEFT]) glColor3f( 1.0f, 0.0f, 0.0f );
		vec3 pointOfImpactB = (g_cube->getRotation() * vec3( -1.0f, -1.0f, 1.0f ) ) + g_cube->getPosition();
		glTranslatef( pointOfImpactB[0], pointOfImpactB[1], pointOfImpactB[2] );
		mat4 subCubeRotationB(g_cube->getRotation());
		glMultMatrixf(glm::value_ptr(subCubeRotationB));
		drawFixedCube(0.25f);
		glColor3f(1,1,1);
	glPopMatrix();
	
	vec3 cubePosition = g_cube->getPosition();
	glTranslatef(cubePosition.x, cubePosition.y, cubePosition.z);
	mat4 cubeRotation(g_cube->getRotation());
	glMultMatrixf(glm::value_ptr(cubeRotation));

	// Draw actual big cube
	glColor3f(1.0f, 1.0f, 1.0f);
	drawFixedCube(1.0f);

	glPopMatrix();
}

GLvoid drawFixedCube(GLfloat a_size)
{
	glBegin(GL_QUADS);
		// Top face
		glNormal3f(0.0f, a_size, 0.0f);
		glTexCoord2f(a_size, a_size); glVertex3f( a_size, a_size, -a_size);
		glTexCoord2f(0.0f, a_size); glVertex3f(-a_size, a_size, -a_size);
		glTexCoord2f(0.0f, 0.0f); glVertex3f(-a_size, a_size,  a_size);
		glTexCoord2f(a_size, 0.0f); glVertex3f( a_size, a_size,  a_size);
		// Bottom face
		glNormal3f(0.0f, -a_size, 0.0f);
		glTexCoord2f(a_size, a_size); glVertex3f(a_size, -a_size, -a_size);
		glTexCoord2f(0.0f, a_size); glVertex3f(-a_size, -a_size, -a_size);
		glTexCoord2f(0.0f, 0.0f); glVertex3f(-a_size, -a_size, a_size);
		glTexCoord2f(a_size, 0.0f); glVertex3f(a_size, -a_size, a_size);
		// Front face
		glNormal3f(0.0f, 0.0f, a_size);
		glTexCoord2f(a_size, a_size); glVertex3f(a_size, a_size, a_size);
		glTexCoord2f(0.0f, a_size); glVertex3f(-a_size, a_size, a_size);
		glTexCoord2f(0.0f, 0.0f); glVertex3f(-a_size, -a_size, a_size);
		glTexCoord2f(a_size, 0.0f); glVertex3f(a_size, -a_size, a_size);
		// Back face
		glNormal3f(0.0f, 0.0f, -a_size);
		glTexCoord2f(a_size, a_size); glVertex3f(a_size, a_size, -a_size);
		glTexCoord2f(0.0f, a_size); glVertex3f(-a_size, a_size, -a_size);
		glTexCoord2f(0.0f, 0.0f); glVertex3f(-a_size, -a_size, -a_size);
		glTexCoord2f(a_size, 0.0f); glVertex3f(a_size, -a_size, -a_size);
		// Left face
		glNormal3f(a_size, 0.0f, 0.0f);
		glTexCoord2f(a_size, a_size); glVertex3f(-a_size, a_size, a_size);
		glTexCoord2f(0.0f, a_size); glVertex3f(-a_size, a_size, -a_size);
		glTexCoord2f(0.0f, 0.0f); glVertex3f(-a_size, -a_size, -a_size);
		glTexCoord2f(a_size, 0.0f); glVertex3f(-a_size, -a_size, a_size);
		// Right face
		glNormal3f(-a_size, 0.0f, 0.0f);
		glTexCoord2f(a_size, a_size); glVertex3f(a_size, a_size, a_size);
		glTexCoord2f(0.0f, a_size); glVertex3f(a_size, a_size, -a_size);
		glTexCoord2f(0.0f, 0.0f); glVertex3f(a_size, -a_size, -a_size);
		glTexCoord2f(a_size, 0.0f); glVertex3f(a_size, -a_size, a_size);
	glEnd();
}

// Draws a grid with defined width, height, and divisions
GLvoid drawGrid() {
	const float width = 80;
	const float height = 80;
	const int	divisions = 100;
	float		incrementX = width / (float)divisions;
	float		incrementY = height / (float)divisions;

	glNormal3f(0, 1, 0);
	glColor3f(0, 0, 0);
	for (float x = -width / 2; x < width / 2; x += incrementX) {
		for (float y = -height / 2; y < height / 2; y += incrementY) {
			glBegin(GL_TRIANGLE_STRIP);
				glVertex3f(x + incrementX, 0, y + incrementY);
				glVertex3f(x, 0, y + incrementY);
				glVertex3f(x + incrementX, 0, y);
				glVertex3f(x, 0, y);
			glEnd();
		}
	}
}

GLvoid drawScene(GLfloat a_deltaTime)
{

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	
	establishProjectionMatrix(WINDOW_WIDTH, WINDOW_HEIGHT);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glDisable(GL_BLEND);
	glTranslatef(0, 0, g_zoom);
	glRotatef(g_rotateX, 1, 0, 0);
	glRotatef(g_rotateY, 0, 1, 0);

	// Handle the buttons
	handleButtons(a_deltaTime);

	// Do the physics timesteps
	g_timePassed += a_deltaTime;

	while (g_timePassed > PHYSICS_STEP)
	{
		g_timePassed -= PHYSICS_STEP;
		g_cube->tick(PHYSICS_STEP);
	}
	
	// Draw cube
	drawCube();

	glDisable(GL_LIGHTING);
	setOrtho(WINDOW_WIDTH, WINDOW_HEIGHT);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	drawControls();
	iGLEngine->drawText(100, 460, "Use left and right arrow keys for thrusters.");

	displayFPS();

	glFlush();
	SDL_GL_SwapBuffers();
}

GLvoid drawControls() {
	for (list<Control*>::iterator it = Control::controls.begin(); it != Control::controls.end(); ++it) {
		Control *control = (*it);
		control->drawControl();

		if ((g_currentControl != NULL && g_currentControl != control) || g_isNavigating) continue;

		if (control->updateControl(g_mouseState)) {
			// New current control
			g_currentControl = control;
		}
		else if (control == g_currentControl) {
			// Not current control anymore
			g_currentControl = NULL;
		}
	}
}

GLvoid handleButtons(GLfloat a_deltaTime) {
	for (int i = 0; i < NUM_BUTTONS; ++i) {
		if (g_buttonForce[i]->getDown()) {
			switch(i) {
			case 0:
				g_cube->applyForceLocal(g_forceToApply, vec3(1.0f, 1.0f, 1.0f), a_deltaTime);
				break;
			case 1:
				g_cube->applyForceLocal(g_forceToApply, vec3(1.0f, 0.0f, 1.0f), a_deltaTime);
				break;
			}
		}
	}

	if (g_resetButton->getDown()) {
		g_cube->reset();
		g_timePassed = 0.0f;
	}
}

GLboolean checkInput(GLfloat a_deltaTime)
{
	const long updateTime = 10;

	static int lastX = -1;
	static int lastY = -1;

	if (lastX == -1 && lastY == -1) {
		lastX = g_mouseState.m_x;
		lastY = g_mouseState.m_y;
	}

	int changeX = lastX - g_mouseState.m_x;
	int changeY = lastY - g_mouseState.m_y;
	lastX = g_mouseState.m_x;
	lastY = g_mouseState.m_y;

	if (g_mouseState.m_leftButtonDown && g_currentControl == NULL) {
		SDL_WM_GrabInput(SDL_GRAB_ON);
		g_rotateX -= (float)changeY * ROTATE_SPEED;
		g_rotateY -= (float)changeX * ROTATE_SPEED;
		g_isNavigating = true;
	}
	else if (g_mouseState.m_rightButtonDown && g_currentControl == NULL) {
		SDL_WM_GrabInput(SDL_GRAB_ON);
		g_zoom -= (float)changeX * ZOOM_SPEED;
		g_isNavigating = true;
	}
	else {
		SDL_WM_GrabInput(SDL_GRAB_OFF);
		g_isNavigating = false;
	}

	if (g_keys[SDLK_ESCAPE]) return true;

	// Apply force left and right side of the cube
	if (g_keys[SDLK_RIGHT])
	{
		vec3 appliedForceRight = g_cube->getRotation() * vec3(0.0f, 0.0f, -1.0f);
		g_cube->applyForceLocal(appliedForceRight, vec3(1.0f, 1.0f, 1.0f), a_deltaTime);
	}
	if (g_keys[SDLK_LEFT])
	{
		vec3 appliedForceLeft = g_cube->getRotation() * vec3(0.0f, 0.0f, -1.0f);
		g_cube->applyForceLocal(appliedForceLeft, vec3(-1.0f, -1.0f, 1.0f), a_deltaTime);
	}

	return false;
}

int main(int argc, char **argv) {

	if (SDL_Init(SDL_INIT_VIDEO < 0)) {
		fprintf(stderr, "Unable to initialize SDL: %s", SDL_GetError());
		exit(1);
	}
	if (SDL_SetVideoMode(WINDOW_WIDTH, WINDOW_HEIGHT, 0, SDL_OPENGL) == NULL) {
		fprintf(stderr, "Unable to create OpenGL Scene: %s", SDL_GetError());
		exit(2);
	}

	initGL(WINDOW_WIDTH, WINDOW_HEIGHT);

	g_keys = SDL_GetKeyState(NULL);

	long lastTime = SDL_GetTicks();
	float deltaTime = 0.01f;

	bool isDone = false;
	while (!isDone)
	{
		int newTime = SDL_GetTicks();
		deltaTime = (newTime - lastTime) / 1000.0f;
		lastTime = newTime;

		// Update the mouse state
		SDL_GetMouseState(&g_mouseState.m_x, &g_mouseState.m_y);
		g_mouseState.m_leftButtonDown = SDL_GetMouseState(NULL, NULL) & SDL_BUTTON(1);
		g_mouseState.m_middleButtonDown = SDL_GetMouseState(NULL, NULL) & SDL_BUTTON(2);
		g_mouseState.m_rightButtonDown = SDL_GetMouseState(NULL, NULL) & SDL_BUTTON(3);

		drawScene(deltaTime);

		SDL_Event event;
		while (SDL_PollEvent(&event)) {
			if (event.type == SDL_QUIT) {
				isDone = true;
			}

			g_keys = SDL_GetKeyState(NULL);
		}

		if (checkInput(deltaTime)) {
			isDone = true;
		}
	}

	for (list<Control*>::iterator it = Control::controls.begin(); it != Control::controls.end(); it++) {
		delete (*it);
		it = Control::controls.begin();
	}

	GLEngine::exit();
	SDL_Quit();

	return 0;
}