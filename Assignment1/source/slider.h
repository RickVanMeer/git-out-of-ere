#pragma once

#include "interface_control.h"

class Slider : public Control {
public:
	// -structors //
	Slider(string a_label, float a_min, float a_max, int a_positionX, int a_positionY, int a_width, int a_height);

	// Methods //
	virtual bool updateControl(MouseState &a_state);
	virtual void drawControl();
	virtual string getType();
	virtual bool getDown();

	void setValue(float *a_value);

protected:
	float m_defaultValue;
	float m_min;
	float m_max;
	float *m_value;
	string m_label;
	bool m_isDragging;

};