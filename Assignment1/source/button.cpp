#include "button.h"

Button::Button(string a_label, int a_positionX, int a_positionY, int a_width, int a_height) :
	Control(a_positionX, a_positionY, a_width, a_height) {
	m_label = a_label;
	m_down = false;
}

bool Button::updateControl(MouseState &a_state) {
	Control::updateControl(a_state);

	if (m_inside) {
		if (a_state.m_leftButtonDown) {
			m_down = true;
		}
		else if (m_down) {
			m_down = false;
			return true;
		}
	}

	return false;
}

bool Button::getDown() {
	return m_down;
}

void Button::drawControl() {
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glDisable(GL_TEXTURE_2D);

	GLfloat left, right, up, bottom;
	left = GLfloat(m_positionX);
	right = GLfloat(m_positionX + m_width);
	up = GLfloat(m_positionY);
	bottom = GLfloat(m_positionY + m_height);

	glColor4f(0.7f, 0.7f, 0.7f, 0.8f);
	glBegin(GL_QUADS);
		glVertex2f(left		, up);
		glVertex2f(right	, up);
		glVertex2f(right	, bottom);
		glVertex2f(left		, bottom);
	glEnd();

	if (m_inside) {
		glColor4f(0.2f, 0.2f, 0.7f, 0.8f);
		glLineWidth(2.0f);
	}
	else {
		glColor4f(0.2f, 0.2f, 0.7f, 0.5f);
		glLineWidth(1.0f);
	}
	
	glBegin(GL_LINE_STRIP);
		glVertex2f(left		, up);
		glVertex2f(right	, up);
		glVertex2f(right	, bottom);
		glVertex2f(left		, bottom);
		glVertex2f(left		, up);
	glEnd();

	glColor4f(0.7f, 0.7f, 0.7f, 1.0f);
	int textX = (int)left + (m_width - iGLEngine->getTextWidth(m_label.data())) / 2;
	int textY = (int)up + (m_height - iGLEngine->getTextHeight(m_label.data())) / 2;

	iGLEngine->drawText(textX, textY, m_label.data());
}

string Button::getType() {
	return "button";
}