#pragma once

#ifdef _WIN32
#define WIN32_LEAN_AND_MEAN
	#include <windows.h>
#endif

#if defined (__APPLE__) && defined (__MACH__)
	#include <OpenGL/gl.h>
	#include <OpenGL/glu.h>
#else
	#include <GL/gl.h>
	#include <GL/glu.h>
#endif

#include "GLEngine.h"

#include <list>
#include <string>

using std::list;
using std::string;

struct MouseState {
	int m_leftButtonDown;
	int m_rightButtonDown;
	int m_middleButtonDown;
	int m_x;
	int m_y;

	MouseState() {
		m_leftButtonDown = 0;
		m_rightButtonDown = 0;
		m_middleButtonDown = 0;
		m_x = 0;
		m_y = 0;
	}
};

class Control {
public:
	// -structors //
	Control(int a_positionX, int a_positionY, int a_width, int a_height);
	virtual ~Control();

	// Methods //
	virtual bool updateControl(MouseState &a_state);
	virtual void drawControl() = 0;
	virtual string getType() = 0;
	virtual bool getDown() = 0;
	
	// Getters and setters //
	void setPosition(int a_x, int a_y);
	void setSize(int a_width, int a_height);
	int getWidth();
	int getHeight();


	static list<Control*> controls;

protected:
	// Members //
	bool m_inside;
	int m_positionX;
	int m_positionY;
	int m_width;
	int m_height;

};

Control *addControl(Control* a_control);