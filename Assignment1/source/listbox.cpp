#include "listbox.h"

const int ITEM_HEIGHT = 18;

ListBox::ListBox(int a_positionX, int a_positionY, int a_width, int a_height) :
	Control(a_positionX, a_positionY, a_width, a_height) {
	m_index = 0;
}

bool ListBox::updateControl(MouseState &a_state) {
	Control::updateControl(a_state);

	int x = a_state.m_x;
	int y = a_state.m_y;

	if (m_inside && a_state.m_leftButtonDown) {
		int tempIndex = (y - m_positionY) / ITEM_HEIGHT;

		if (tempIndex >= 0 && tempIndex < (int)m_items.size()) {
			m_index = tempIndex;
			return true;
		}
	}

	return false;
}

bool ListBox::getDown() {
	return false;
}

void ListBox::drawControl() {
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glDisable(GL_TEXTURE_2D);

	GLfloat left, right, up, bottom;
	left = GLfloat(m_positionX);
	right = GLfloat(m_positionX + m_width);
	up = GLfloat(m_positionY);
	bottom = GLfloat(m_positionY + m_height);

	glColor4f(0.7f, 0.7f, 0.7f, 0.8f);
	glBegin(GL_QUADS);
		glVertex2f(left		, up);
		glVertex2f(right	, up);
		glVertex2f(right	, bottom);
		glVertex2f(left		, bottom);
	glEnd();

	if (m_inside) {
		glColor4f(0.2f, 0.2f, 0.7f, 0.8f);
		glLineWidth(2.0f);
	}
	else {
		glColor4f(0.2f, 0.2f, 0.7f, 0.5f);
		glLineWidth(1.0f);
	}
	
	glBegin(GL_LINE_STRIP);
		glVertex2f(left		, up);
		glVertex2f(right	, up);
		glVertex2f(right	, bottom);
		glVertex2f(left		, bottom);
		glVertex2f(left		, up);
	glEnd();

	if (m_index >= 0) {
		glColor4f(0.3f, 0.3f, 1.0f, 0.5f);

		int currentY =  m_positionY + m_index * ITEM_HEIGHT;
		bottom = currentY + ITEM_HEIGHT;
		glBegin(GL_QUADS);
			glVertex2f(left		, currentY);
			glVertex2f(right	, currentY);
			glVertex2f(right	, bottom);
			glVertex2f(left		, bottom);
		glEnd();
	}

	glColor4f(0.7f, 0.7f, 0.7f, 1.0f);
	for (int i = 0; i < (int)m_items.size(); ++i) {
		iGLEngine->drawText(m_positionX + 2, m_positionY + 2 + i * ITEM_HEIGHT, m_items[i].data());
	}
}

string ListBox::getType() {
	return "listbox";
}

void ListBox::addItem(string a_item) {
	m_items.push_back(a_item);
}

void ListBox::removeItem(int a_index) {
	vector<string>::const_iterator it = m_items.begin();
	vector<string>::const_iterator endItems = m_items.end();
	int i = 0;
	for (; it != endItems; ++it) {
		if (i == a_index) {
			m_items.erase(it);
			break;
		}
		++i;
	}

	if (m_index >= (int)m_items.size()) {
		m_index = (int)m_items.size() - 1;
	}
}

void ListBox::setCurrent(int a_index) {
	m_index = a_index;
}

int ListBox::getIndex() {
	return m_index;
}

int ListBox::getCount() {
	return (int)m_items.size();
}